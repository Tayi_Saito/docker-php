import './App.css';
import React from 'react';
import { BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Auth from "./containers/auth";
import Links from "./containers/links";
import Themes from "./containers/themes";
import Chats from "./containers/chats";

function App() {
    return (
        <div className="App">

            <main className="">
                <div className="">
                    <BrowserRouter>
                        <Switch>
                            <Route exact path="/chats" component={() => <Chats />} />
                            <Route exact path="/themes" component={() => <Themes />} />
                            <Route exact path="/" component={() => <Links />}/>
                            <Redirect to="/" />
                        </Switch>
                    </BrowserRouter>
                </div>
            </main>
        </div>
    );
}

export default App;
