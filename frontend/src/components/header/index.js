import React from 'react';
import './header.css';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';

function Header() {
    return (
        <header>
            <div>
                <Row className="header">
                    <Col span={6}>
                        <ul className="menu">
                            <li><Link to="/">Links</Link></li>
                            <li><Link to="/chats">Chats</Link></li>
                            <li><Link to="/themes">Themes</Link></li>
                        </ul>
                    </Col>
                </Row>
            </div>
        </header>
    );
}

export default Header;