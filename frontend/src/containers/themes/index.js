import './themes.css';
import React from 'react';
import Header from "../../components/header";

export default class Themes extends React.Component {
	state = {
		loading: true,
		data: null
	};

	async componentDidMount() {
		const url = "http://" + window.location.host + "/api/themes";
		const response = await fetch(url, {
			method:  "GET",
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json"
			}
		}).catch(error => {
				console.log('Error fetch-request: ',error.message);
				this.setState({loading: false});
			});
		try {
			const data = await response.json();
			if (data.themes) {
				this.setState({data: data.themes});
			} else this.setState({loading: false});
		} catch (err) {
			console.log(err);
		}
		this.setState({loading: false});
	}

	render() {
		return (
			<div>
				<Header/>
				<div className="main-info pt-4">
					<h1 className="mb-3 flex-info">Themes</h1>
					{this.state.loading || !this.state.data ? (
						<div>
							<div>Loading...</div>
						</div>
					) : (
						<ul className="mb-3">
							{this.state.data.map(item => (
							<li className="list-group-item-dark"
								key={item.id.toString()}>
								<p className="h4">Theme: {item.name}</p>
							</li>
						))}
						</ul>
					)}
				</div>
			</div>
		);
	}
}