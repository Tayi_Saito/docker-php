import './auth.css';
import React from 'react';
import {Formik, Form, Field} from 'formik';
import {Links} from "../links";
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Header from "../../components/header";

function validateEmail(value) {
    let error;
    if (!value) {
        error = '*Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
        error = 'Invalid email address';
    }
    return error;
}

function validatePassword(value) {
    let error;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    if (!value) {
        error = '*Required';
    } else if (value.length < 6) {
        error = "*Password must be 6 characters long.";
    } else if (!passwordRegex.test(value)) {
        error = "*Invalid password. Must contain one number and one letter.";
    }
    return error;
}

export default class Auth extends React.Component {
    state = {
        sending: false,
        error: null,
        userData: null,
        serverData: null
    };

    async sendMessageAuth() {
        const url = "http://" + window.location.host + "/api/auth";
        try {
            const response = await fetch(url, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(this.state.userData),
            }).catch(error => console.log('Error fetch-request: ', error.message));
            const data = response.json();
            console.log(data);
            this.setState({sending: false, serverData: data.token});

        } catch (error) {
            console.log('error: ', error);
        }
    }


    render() {
        return (
            <div>
                <Header/>
                {!this.state.serverData ? (
                    <div className="main-info pt-4">
                        <h1>Sign in</h1>
                        <Formik
                            initialValues={{
                                email: '',
                                password: '',
                            }}
                            onSubmit={values => {
                                this.setState({sending: true, userData: values});
                                this.sendMessageAuth();
                            }}
                        >
                            {({errors, touched, validateField, validateForm}) => (
                                <Form className="form-size">
                                    <Field name="email" validate={validateEmail}
                                           placeholder="Email"
                                           className="form-control mt-3"/>
                                    {errors.email && touched.email && <div>{errors.email}</div>}

                                    <Field name="password" validate={validatePassword}
                                           type="password"
                                           placeholder="Password"
                                           className="form-control mt-3"/>
                                    {errors.password && touched.password && <div>{errors.password}</div>}
                                    {this.state.sending ? (<div>Waitting...</div>) : (
                                        <div>
                                            <button type="submit" className="btn btn-link App-link m-3">
                                                Submit
                                            </button>
                                            <div>{this.state.error}</div>
                                        </div>
                                    )}
                                </Form>
                            )}
                        </Formik>
                    </div>
                ) : (
                    <div>

                        <Redirect to="/profile"/>
                    </div>
                )
                }
            </div>

        );
    }
}