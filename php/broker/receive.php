<?php
use brokers\SimpleReceiver;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . "/SimpleReceiver.php";

$receiver = new SimpleReceiver();
$receiver->listen();
