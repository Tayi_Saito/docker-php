<?php

require_once('BaseAPIClass.php');

$SALT =  "shi389ejldimsopepegakappa123";
//$SALT2 = "imsopepegaimsopepegaimsopepega";

class Users extends BaseAPIClass {
    public static $class_name = "Users";
    public static $base_route = "/users/";
    public static $http_methods = [
        "exists" => EpiRoute::httpGet,
        "create" => EpiRoute::httpPost,
        "startSession" => EpiRoute::httpPost,
        "checkSession" => EpiRoute::httpGet,
    ];

    private static function hidePassword($password) {
        global $SALT;
        return password_hash($password, PASSWORD_BCRYPT, ['salt' => $SALT]);
//        return password_hash($password, PASSWORD_BCRYPT);
    }

    private static function createSessionID($login, $password) {
//        global $SALT;
//        $phash = static::hidePassword($password);
//        $sessID = password_hash($login . strval(time()), PASSWORD_BCRYPT, ['salt' => $SALT]);
        $a = $login . $password . strval(time());
        $sessID = password_hash($a,PASSWORD_BCRYPT);
//        $sessID = $phash ^ $lhash;
        return $sessID;
    }

    // 'login', 'password'
    // 1 = login correct, password correct
    // 2 = login correct, password incorrect
    // 3 = login incorrect, password incorrect
    // user => USERS
    public static function exists() {
        $arr = static::getRequestArray('exists');
        $answer = getAnswerPrototype();
        if (!isset($arr['login'])) {
            $answer['message'] = 'You must enter login';
            return $answer;
        }
        if (!isset($arr['password'])) {
            $answer['message'] = 'You must enter password';
            return $answer;
        }
        $login = $arr['login'];
        $password_hash = static::hidePassword($arr['password']);
        global $dbconn;

        if (!($query = $dbconn->prepare("SELECT * FROM users WHERE login=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("s", $login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->fetchAll(PDO::FETCH_ASSOC))) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (count($res) == 1) {
//            $res->data_seek(0);
            $arr = $res[0];
            if ($password_hash == $arr['password_hash']) {
                $answer['status'] = 1;
                $answer['user'] = $arr;
            }
            else {
                $answer['status'] = 2;
                $answer['message'] = 'Password is incorrect';
            }
        }
        else {
            $answer['status'] = 3;
            $answer['message'] = 'No user with this login found';
        }
        return $answer;
    }

    public static function existsSFW($arr) {
        unset($arr['password_hash']);
        unset($arr['session_id']);
        return $arr;
    }

    // 'login', 'password'
    // 1 = created new user with this login and password
    // 2 = login already exists
    public static function create() {
        $arr = static::getRequestArray('create');
        $answer = getAnswerPrototype();

        $res = static::apiInvoke('exists', $arr);
        switch ($res['status']) {
            case 3:
                break;
            case 1:
            case 2:
                $answer['status'] = 2;
                $answer['message'] = 'This login is already taken';
                return $answer;
            case 5:
            default:
                return $res;
        }

        global $dbconn;
        $login = $arr['login'];
        $password_hash = static::hidePassword($arr['password']);

        if (!($query = $dbconn->prepare("INSERT INTO users(login,password_hash) VALUES (?,?)"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("ss", $login,$password_hash)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$login, $password_hash])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $answer['status'] = 1;
        $answer['message'] = 'User successfully created';
        return $answer;
    }

    // 'login', 'password'
    // 1 = created new session for user
    // 2 = user not found
    // token => createSessionID()
    public static function startSession() {
        $arr = static::getRequestArray('startSession');
        $answer = getAnswerPrototype();

        $res = static::apiInvoke("exists", $arr);
        switch ($res['status']) {
            case 1:
                break;
            case 2:
            case 3:
                $answer['status'] = 2;
                $answer['message'] = 'User not found';
                return $answer;
            case 5:
            default:
                return $res;
        }

        global $dbconn;
        $login = $arr['login'];
        $session_id = static::createSessionID($login, $arr['password']);

        if (!($query = $dbconn->prepare("UPDATE users SET session_id=? WHERE login=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("ss", $session_id,$login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$session_id, $login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $answer['status'] = 1;
        $answer['message'] = 'Created new session';
        $answer['token'] = $session_id;
        return $answer;
    }

    // 'token'
    // 1 = token correct
    // 2 = no token found
    // 3 = two or more equal tokens
    // user => USERS
    public static function checkSession() {
        $arr = static::getRequestArray('checkSession');
        $answer = getAnswerPrototype();
        if (!isset($arr['token'])) {
            $answer['message'] = 'You must enter token';
            return $answer;
        }
        $token = $arr['token'];
        global $dbconn;

        if (!($query = $dbconn->prepare("SELECT * FROM users WHERE session_id=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("s", $token)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$token])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->fetchAll(PDO::FETCH_ASSOC))) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (count($res) == 1) {
//            $res->data_seek(0);
            $answer['status'] = 1;
            $answer['message'] = 'Session is correct';
            $answer['user'] = $res[0];
            unset($answer['user']['password_hash']);
        }
        else if (count($res) == 0) {
            $answer['status'] = 2;
            $answer['message'] = 'User not found';
        }
        else {
            // SAME TOKENS?
            $answer['status'] = 3;
            $answer['message'] = 'Something went wrong';
        }
        return $answer;
    }
}
