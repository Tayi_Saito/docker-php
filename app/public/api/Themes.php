<?php

require_once('BaseAPIClass.php');

class Themes extends BaseAPIClass {
    public static $class_name = "Themes";
    public static $base_route = "/themes/";
    public static $http_methods = [
        "find" => EpiRoute::httpGet,
        "getall" => EpiRoute::httpGet,
    ];

    public static function normalize_image_url($res) {
        $res['image'] = getenv('IMAGES_DIR') . static::$base_route . $res['image'];
        return $res;
    }

    // name
    // 1 = get theme by name
    // 2 = no theme found
    // 3 = more than one theme found
    // theme => THEMES
    public static function find() {
        $arr = static::getRequestArray('find');
        $answer = getAnswerPrototype();
        if (!isset($arr['name'])) {
            $answer['message'] = 'You must enter name';
            return $answer;
        }
        $name = $arr['name'];

        global $dbconn;
        if (!($query = $dbconn->prepare("SELECT * FROM themes WHERE name=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("s", $name)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$name])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (count($res) == 1) {
//            $res->data_seek(0);
            $answer['status'] = 1;
            $answer['theme'] = static::normalize_image_url($res[0]);
        }
        else if (count($res) == 0) {
            $answer['status'] = 2;
            $answer['message'] = 'No theme with this name found';
        }
        else {
            $answer['status'] = 3;
            $answer['message'] = 'More than one theme with this name found';
        }
        return $answer;
    }

    // 1 = get all themes
    // themes => [THEMES]
    public static function getall() {
        $answer = getAnswerPrototype();
        global $dbconn;

        if (!($query = $dbconn->prepare("SELECT * FROM themes ORDER BY 2 ASC"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

        if (!$query->execute()) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        foreach ($res as $num => $theme) {
            $res[$num] = static::normalize_image_url($theme);
        }
        $answer['status'] = 1;
        $answer['themes'] = $res;
        return $answer;
    }
}
