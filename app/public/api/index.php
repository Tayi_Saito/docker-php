<?php

$BASE_DIR = '../../';

include_once($BASE_DIR . 'db/db.php');
include_once($BASE_DIR . 'epi/Epi.php');

Epi::setPath('base',$BASE_DIR . 'epi');
Epi::init('api');

//header("Content-Type:application/json; charset=UTF-8");
header("X-Instance-Name: " . getenv('INSTANCE_NAME'));

include_once('Users.php');
include_once('Themes.php');
include_once('Chats.php');
include_once('Links.php');

getApi()->get('/users/exists', array('Users', 'exists'), EpiApi::internal);
getApi()->post('/users/create', array('Users', 'create'), EpiApi::internal);
getApi()->post('/users/startSession', array('Users', 'startSession'), EpiApi::internal);
getApi()->get('/users/checkSession', array('Users', 'checkSession'), EpiApi::internal);

getApi()->get('/themes/find', array('Themes', 'find'), EpiApi::internal);

getApi()->post('/chats/create', array('Chats', 'create'), EpiApi::internal);
getApi()->post('/chats/delete', array('Chats', 'delete'), EpiApi::internal);

getApi()->get('/chats/count', array('Chats', 'count'), EpiApi::internal);
getApi()->get('/chats/findMatching', array('Chats', 'findMatching'), EpiApi::internal);
getApi()->get('/chats/findUser', array('Chats', 'findUser'), EpiApi::internal);
getApi()->post('/chats/addUser', array('Chats', 'addUser'), EpiApi::internal);
getApi()->post('/chats/removeUser', array('Chats', 'removeUser'), EpiApi::internal);

getApi()->get('/links/getall', array('Links', 'getall'), EpiApi::internal);
getApi()->post('/links/create', array('Links', 'create'), EpiApi::internal);

// =========================================================================================

getApi()->get('/themes', array('Themes', 'getall'), EpiApi::external);
getApi()->get('/chats', array('Chats', 'findMatching'), EpiApi::external);
getApi()->get('/links', array('Links', 'getall'), EpiApi::external);
getApi()->get('/links/(\d+)', array('Links', 'get'), EpiApi::external);
getApi()->put('/links/(\d+)', array('Links', 'update'), EpiApi::external);

getApi()->post('/register', 'doUsersRegister', EpiApi::external);
getApi()->get('/auth', 'doUsersAuth', EpiApi::external);
getApi()->get('/authToken', 'doUsersAuthToken', EpiApi::external);
getApi()->post('/joinChat', 'doUsersJoinChat', EpiApi::external);
getApi()->post('/leaveChat', 'doUsersLeaveChat', EpiApi::external);
getApi()->post('/updateUser', 'doUsersUpdate', EpiApi::external);

getRoute()->run();


// login, password
// 1 = registered
// 2 = no
// user => USERS
// token => Users.createSessionID()
function doUsersRegister() {
    $arr = Users::getRequestArray('create');
    $answer = getAnswerPrototype();

    $res = Users::apiInvoke("create", $arr);
    switch ($res['status']) {
        case 1:
            $answer['status'] = 1;
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! This login is already taken.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    if ($answer['status'] != 1) {
        return $answer;
    }

    $res = Users::apiInvoke("startSession", $arr);
    switch ($res['status']) {
        case 1:
            $answer['token'] = $res['token'];
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! User is created, but session can not be started.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    if ($answer['status'] != 1) {
        return $answer;
    }

    $res = Users::apiInvoke("exists", $arr);
    switch ($res['status']) {
        case 1:
            $answer['message'] = 'Success!';
            $answer['user'] = Users::existsSFW($res['user']);
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! Session is started, but user can not be found.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    return $answer;
}


// login, password
// 1 = authorized
// 2 = no
// user => USERS
// token => Users.createSessionID()
function doUsersAuth() {
    $arr = Users::getRequestArray('exists');
    $answer = getAnswerPrototype();
    $res = Users::apiInvoke('exists', $arr);
    switch ($res['status']) {
        case 1:
            $answer['status'] = 1;
            $answer['message'] = 'Success!';
            $answer['user'] = Users::existsSFW($res['user']);
            break;
        case 2:
        case 3:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! Login or password is incorrect.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    if ($answer['status'] != 1) {
        return $answer;
    }

    $res = Users::apiInvoke('startSession', $arr);
    switch ($res['status']) {
        case 1:
            $answer['token'] = $res['token'];
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! Login or password changed while creating session.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    return $answer;
}

// token
// 1 = authorized
// 2 = no
function doUsersAuthToken() {
    $arr = Users::getRequestArray('checkSession');
    $answer = getAnswerPrototype();
    $res = Users::apiInvoke('checkSession', $arr);
    switch ($res['status']) {
        case 1:
            $answer['status'] = 1;
            $answer['message'] = 'Success!';
            $answer['user'] = $res['user'];
            break;
        case 2:
        case 3:
            $answer['status'] = 2;
            $answer['message'] = 'Failure! Token is incorrect.';
            break;
        case 5:
        default:
            $answer = $res;
    }
    return $answer;
}

// token, chat_id
// 1 = joined
// 2 = already in
// 3 = unauthorized
function doUsersJoinChat() {
    $arr = Chats::getRequestArray('addUser');
    $answer = getAnswerPrototype();

    $res = getApi()->invoke('/authToken', EpiRoute::httpGet, $arr);
    switch ($res['status']) {
        case 1:
            break;
        case 2:
            $answer['status'] = 3;
            $answer['message'] = 'Unauthorized.';
            return $answer;
        case 5:
        default:
            return $res;
    }
    $arr['login'] = $res['user']['login'];

    $res = Chats::apiInvoke('findUser', $arr);
    switch ($res['status']) {
        case 2:
            break;
        case 1:
            $answer['status'] = 2;
            $answer['message'] = 'User already in a chat.';
            return $answer;
        case 3:
            $answer['status'] = 3;
            $answer['message'] = 'User disappeared while searching for them in chats.';
            return $answer;
        case 5:
        default:
            return $res;
    }

    if (!isset($arr['chat_id'])) {
        $res = Chats::apiInvoke('findMatching', $arr);
        switch ($res['status']) {
            case 1:
                break;
            case 5:
            default:
                return $res;
        }
        if (count($res['chats']) > 0) {
            $arr['chat_id'] = $res['chats'][0]['id'];
        } else {
            $res = Chats::apiInvoke('create', $arr);
            switch ($res['status']) {
                case 1:
                    $arr['chat_id'] = $res['chat_id'];
                    break;
                case 5:
                default:
                    return $res;
            }
        }
    }

    $res = Chats::apiInvoke('addUser', $arr);
    switch ($res['status']) {
        case 1:
            $answer['status'] = 1;
            $answer['message'] = 'User successfully joined the chat.';
            break;
        case 2:
        case 5:
        default:
            return $res;
    }

    return $answer;
}

// token
// 1 = left
// 2 = is not in a chat
// 3 = unauthorized
function doUsersLeaveChat() {
    $arr = Chats::getRequestArray('removeUser');
    $answer = getAnswerPrototype();

    $res = getApi()->invoke('/authToken', EpiRoute::httpGet, $arr);
    switch ($res['status']) {
        case 1:
            break;
        case 2:
            $answer['status'] = 3;
            $answer['message'] = 'Unauthorized.';
            return $answer;
        case 5:
        default:
            return $res;
    }
    $arr['login'] = $res['user']['login'];

    $res = Chats::apiInvoke('findUser', $arr);
    switch ($res['status']) {
        case 1:
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'User is not in a chat.';
            return $answer;
        case 3:
            $answer['status'] = 3;
            $answer['message'] = 'User disappeared while searching for them in chats.';
            return $answer;
        case 5:
        default:
            return $res;
    }
    $arr['chat_id'] = $res['chat_id'];

    $res = Chats::apiInvoke('removeUser', $arr);
    switch ($res['status']) {
        case 1:
            $answer['status'] = 1;
            $answer['message'] = 'User successfully left chats.';
            break;
        case 2:
            $answer['status'] = 3;
            $answer['message'] = 'User disappeared while removing them from chats.';
            break;
        case 5:
        default:
            return $res;
    }

    $res = Chats::apiInvoke('count', $arr);
    switch ($res['status']) {
        case 1:
            break;
        case 2:
            Chats::apiInvoke('delete', $arr);
            break;
        case 5:
        default:
            return $res;
    }
    return $answer;
}

// token, link
// 1 = user updated
// 2 = unauthorized
function doUsersUpdate() {
    $arr = Users::getRequestArray('create');
    $answer = getAnswerPrototype();
    if (!isset($arr['link'])) {
        $answer['message'] = 'You must enter link';
        return $answer;
    }

    $res = getApi()->invoke('/authToken', EpiRoute::httpGet,
        array('_GET' => $arr));
    switch ($res['status']) {
        case 1:
            break;
        case 2:
            $answer['status'] = 2;
            $answer['message'] = 'Unauthorized.';
            return $answer;
        case 5:
        default:
            return $res;
    }
    $login = $res['user']['login'];
    $arr['login'] = $login;
    $link = $arr['link'];

    $res = Links::apiInvoke('create', $arr);
    if ($res['status'] != 1) {
        return $res;
    }
    $link_id = $res['link_id'];
    $arr['link_id'] = $link_id;

    global $dbconn;
    if (!($query = $dbconn->prepare("UPDATE users SET link_id=? WHERE login=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        $answer['message'] = 'Prepare failed';
        $answer['error'] = $dbconn->errorInfo();
        return $answer;
    }

//    if (!$query->bind_param("is", $link_id,$login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Binding parameters failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

    if (!$query->execute([$link_id, $login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        $answer['message'] = 'Execute failed';
        $answer['error'] = $query->errorInfo();
        return $answer;
    }

    send(['link_id' => $link_id, 'link' => $link]);

    $answer['status'] = 1;
    $answer['link_id'] = $link_id;
    return $answer;
}
