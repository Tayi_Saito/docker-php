<?php

require_once ('BaseAPIClass.php');

class ChatCriterion {
    public $varType;
    public $default;
    public $sign;
    public $required;

    public function __construct($varType, $default, $sign='=', $required=false)
    {
        $this->varType = $varType;
        $this->default = $default;
        $this->sign = $sign;
        $this->required = $required;
    }
}

$CHAT_CRITERIONS = [
    "theme_id" => new ChatCriterion("i", null, '=', true),
    "age_min" => new ChatCriterion("i", 0, '>='),
    "age_max" => new ChatCriterion("i", null, '<=')
];

class Chats extends BaseAPIClass {
    public static $class_name = "Chats";
    public static $base_route = "/chats/";
    public static $http_methods = [
        "create" => EpiRoute::httpPost,
        "count" => EpiRoute::httpGet,
        "delete" => EpiRoute::httpPost,
        "findMatching" => EpiRoute::httpGet,
        "findUser" => EpiRoute::httpGet,
        "addUser" => EpiRoute::httpPost,
        "removeUser" => EpiRoute::httpPost,
    ];

    // theme_name, ?age_min, ?age_max
    // 1 = chat created
    // chat_id
    public static function create() {
        $arr = static::getRequestArray('create');
        $answer = getAnswerPrototype();

        $res = Themes::apiInvoke('find', ['name' => $arr['theme_name']]);
        if ($res['status'] != 1) {
            return $res;
        }
        $arr['theme_id'] = $res['theme']['id'];

        global $CHAT_CRITERIONS;
        $stmt = '';
        $stmt2 = '';
        $vars = [];
//        $varTypes = '';
        foreach ($CHAT_CRITERIONS as $name => $criterion) {
            if (!isset($arr[$name])) {
                if ($criterion->required) {
                    $answer['message'] = 'Parameter undefined: ' . $name;
                    return $answer;
                }
                $arr[$name] = $criterion->default;
            }
            if (strlen($stmt)) {
                $stmt .= ',';
                $stmt2 .= ',';
            }
            $stmt .= $name;
            $stmt2 .= '?';
            array_push($vars, $arr[$name]);
//            $varTypes .= $criterion->varType;
        }
        if (strlen($stmt)) {
            $stmt = '(' . $stmt . ') VALUES(' . $stmt2 . ')';
        }
        $stmt = "INSERT INTO chats" . $stmt;

        global $dbconn;
        if (!($query = $dbconn->prepare($stmt))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param($varTypes, ...$vars)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute($vars)) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = static::apiInvoke('findMatching', $arr);
        switch ($res['status']) {
            case 1:
                foreach ($res['chats'] as $num => $chat) {
                    $answer['chat_id'] = $chat['id'];
                }
                break;
            case 2:
                $answer['message'] = 'Chat can not be created';
                return $answer;
            case 5:
            default:
                return $res;
        }

        $answer['status'] = 1;
        $answer['message'] = 'Chat is successfully created';
        return $answer;
    }

    // chat_id
    // 1 = chat deleted
    // 2 = no chat found
    public static function delete() {
        $arr = static::getRequestArray('delete');
        $answer = getAnswerPrototype();

        global $dbconn;
        if (!($query = $dbconn->prepare("DELETE FROM chats WHERE id=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param($varTypes, ...$vars)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$arr['chat_id']])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $answer['status'] = 1;
        $answer['message'] = 'Chat is successfully deleted';
        return $answer;
    }

    // chat_id
    // 1 = chat is found
    // 2 = no chat found
    // count
    public static function count() {
        $arr = static::getRequestArray('count');
        $answer = getAnswerPrototype();
        if (!isset($arr['chat_id'])) {
            $answer['message'] = 'You must enter chat_id';
            return $answer;
        }
        $chat_id = $arr['chat_id'];


        global $dbconn;
        if (!($query = $dbconn->prepare("SELECT COUNT(*) FROM chats_users WHERE chat_id=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("ss", $chat_id, $login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$chat_id])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetch();
        if ($res[0] > 0) {
            $answer['status'] = 1;
            $answer['count'] = $res;
        }
        else {
            $answer['status'] = 2;
            $answer['count'] = $res;
        }

        return $answer;
    }

    // ?theme_name, ?age_min, ?age_max
    // 1 = chat list
    // chats => [CHATS]
    public static function findMatching() {
        $arr = static::getRequestArray('findMatching');
        $answer = getAnswerPrototype();

        if (isset($arr['theme_name'])) {
            $res = Themes::apiInvoke('find', ['name' => $arr['theme_name']]);
            if ($res['status'] != 1) {
                return $res;
            }
            $arr['theme_id'] = $res['theme']['id'];
        }

        global $CHAT_CRITERIONS;
        $stmt = "";
        $var_types = "";
        $vars = [];
        foreach ($CHAT_CRITERIONS as $criterion_name => $criterion) {
            if (isset($arr[$criterion_name])) {
                if (!strlen($stmt)) {
                    $stmt .= " WHERE ";
                }
                else {
                    $stmt .= " AND ";
                }
                $stmt .= $criterion_name . $criterion->sign . "?";
                $var_types .= $criterion->varType;
                array_push($vars, $arr[$criterion_name]);
            }
        }
        $stmt = "SELECT * FROM chats" . $stmt . " ORDER BY id";

        global $dbconn;
        if (!($query = $dbconn->prepare($stmt))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if ($vars && !$query->bind_param($var_types, ...$vars)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute($vars)) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->fetchAll(PDO::FETCH_ASSOC))) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        $answer['status'] = 1;
        $answer['message'] = 'Matching chats';
        $answer['chats'] = $res;
        return $answer;
    }

    // login
    // 1 = user found in chat
    // 2 = user not found in chat
    // 3 = user does not exist
    // chat_id
    public static function findUser() {
        $arr = static::getRequestArray('findUser');
        $answer = getAnswerPrototype();
        if (!isset($arr['login'])) {
            $answer['message'] = 'You must enter login';
            return $answer;
        }
        $login = $arr['login'];
        $arr['password'] = '';

        global $dbconn;
        $res = Users::apiInvoke('exists', $arr);
        switch ($res['status']) {
            case 1:
            case 2:
                break;
            case 3:
                $answer['status'] = 3;
                $answer['message'] = 'User does not exist';
                return $answer;
            case 5:
            default:
                return $res;
        }

        if (!($query = $dbconn->prepare("SELECT * FROM chats_users WHERE users_login=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("s", $login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Getting result set failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (count($res) == 1) {
//            $res->data_seek(0);
            $answer['status'] = 1;
            $answer['message'] = 'User is in a chat';
            $answer['chat_id'] = $res[0]['chat_id'];
        }
        else {
            $answer['status'] = 2;
            $answer['message'] = 'User is not in a chat';
        }
        return $answer;
    }

    // login, chat_id
    // 1 = user added to chat
    // 3 = user does not exist
    public static function addUser() {
        $arr = static::getRequestArray('addUser');
        $answer = getAnswerPrototype();
        if (!isset($arr['login'])) {
            $answer['message'] = 'You must enter login';
            return $answer;
        }
        if (!isset($arr['chat_id'])) {
            $answer['message'] = 'You must enter chat_id';
            return $answer;
        }
        $login = $arr['login'];
        $chat_id = $arr['chat_id'];
        $arr['password'] = '';

        $res = Users::apiInvoke('exists', $arr);
        switch ($res['status']) {
            case 1:
            case 2:
                break;
            case 3:
                $answer['status'] = 3;
                $answer['message'] = 'User does not exist';
                return $answer;
            case 5:
            default:
                return $res;
        }

        global $dbconn;
        if (!($query = $dbconn->prepare("INSERT INTO chats_users(chat_id,users_login) VALUES(?,?)"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//        if (!$query->bind_param("ss", $chat_id, $login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$chat_id, $login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $answer['status'] = 1;
        $answer['message'] = 'User successfully joined the chat';
        return $answer;
    }

    // login
    // 1 = user removed from chat
    // 2 = user does not exist
    public static function removeUser() {
        $arr = static::getRequestArray('removeUser');
        $answer = getAnswerPrototype();
        if (!isset($arr['login'])) {
            $answer['message'] = 'You must enter login';
            return $answer;
        }
        $login = $arr['login'];
        $arr['password'] = '';

        $res = Users::apiInvoke('exists', $arr);
        switch ($res['status']) {
            case 1:
            case 2:
                break;
            case 3:
                $answer['status'] = 2;
                $answer['message'] = 'User does not exist';
                return $answer;
            case 5:
            default:
                return $res;
        }

        global $dbconn;
        if (!($query = $dbconn->prepare("DELETE FROM chats_users WHERE users_login=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//        if (!$query->bind_param("s", $login)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//            $answer['message'] = 'Binding parameters failed';
//            $answer['error'] = $query->errorInfo();
//            return $answer;
//        }

        if (!$query->execute([$login])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $answer['status'] = 1;
        $answer['message'] = 'User successfully left chats';
        return $answer;
    }
}
